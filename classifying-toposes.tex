\section{Classifying toposes and Morita equivalence}

For any topos $\E$ and geometric theory $T$,
the models of $T$ in $\E$ form a category
which we simply denote by $T(\E)$.
Moreover, any geometric morphism $f : \E' \to \E$
yields a functor
\[ f^* : T(\E) \to T(\E') \]
by applying the inverse image part $f^*$ of $f$
to the objects, morphisms and subobjects
(for sorts, function symbols and relation symbols)
of which a model consists.

Here it is crucial that we have restricted our attention
to geometric theories instead of all first-order theories.
For example, the interpretation of infinitary conjunction
would have been intersection of infinitely many subobjects.
This is an infinite limit,
but $f^*$ is only required to preserve finite limits.
However, $f^*$ preserves arbitrary colimits
(since it is a left adjoint)
and also the epi-mono factorization of morphisms
(since this can be characterized using finite limits and colimits),
which is all that is needed to construct infinite unions of subobjects,
so $f^*$ does preserve the interpretation of infinite disjuctions.

We denote by $\Geom(\E', \E)$ the category of geometric morphisms
between toposes $\E'$ and $\E$.

% TODO: motivation for the definition?

\begin{definition}
	A \emph{classifying topos} for a geometric theory $T$
	is a Grothendieck topos $\Set[T]$ such that
	for every Grothendieck topos $\E$
	there is an equivalence of categories
	\[ \Geom(\E, \Set[T]) \simeq T(\E) \]
	and this equivalence is natural in $\E$,
	meaning that for every geometric morphism $f : \E' \to \E$
	the diagram of functors
	\[ \begin{tikzcd}
		\Geom(\E, \Set[T]) \ar[r, "\simeq"] \ar[d, "\placeholder \circ f"]
		& T(\E) \ar[d, "f^*"] \\
		\Geom(\E', \Set[T]) \ar[r, "\simeq"]
		& T(\E')
	\end{tikzcd} \]
	commutes up to isomorphism.
\end{definition}

The model corresponding to the identity functor on $\Set[T]$
under the equivalence
\[ \Geom(\Set[T], \Set[T]) \simeq T(\Set[T]) \]
is then called a \emph{universal model} of $T$ and denoted $U_T$.
And indeed, the equivalence $\Geom(\E, \Set[T]) \simeq T(\E)$
is then (up to isomorphism) given by $f \mapsto f^*U_T$,
as can be seen by setting $\Set[T]$ for $\E$ in the naturality square.
So in particular, any model $M$ of $T$ in any topos
is (up to isomorphism) the pullback of $U_T$
along a unique (up to isomorphism) geometric morphism to $\Set[T]$.

We next ask the question when to consider
two theories $T$ and $T'$ \enquote{essentially the same}.
Of course, if $T$ and $T'$ share the same signature,
then we can simply ask if the axioms of each one
can be deduced from the axioms of the other.
(That is, if each is a quotient of the other.)
Then (by Proposition \ref{proposition-soundness})
they also have the same models in any topos.
But we also want to compare theories with different signatures.
The appropriate general notion of
\enquote{having the same models in all toposes}
is given by Definition~\ref{definition-Morita-equivalence}.

\begin{definition}
  \label{definition-Morita-equivalence}
	Two geometric theories $T$ and $T'$ are \emph{Morita-equivalent}
	if for every Gro\-then\-dieck topos $\E$
	there is an equivalence of categories
	\[ T(\E) \simeq T'(\E), \]
	and this equivalence is natural in $\E$,
	meaning that for every geometric morphism $f : \E' \to \E$
	the diagram of functors
	\[ \begin{tikzcd}
		T(\E) \ar[r, "\simeq"] \ar[d, "f^*"]
		& T'(\E) \ar[d, "f^*"] \\
		T(\E') \ar[r, "\simeq"]
		& T'(\E')
	\end{tikzcd} \]
	commutes up to isomorphism.
\end{definition}

\begin{lemma}
	Given two theories $T$, $T'$
	and classifying toposes $\Set[T]$, $\Set[T']$,
	$T$ and $T'$ are Morita-equivalent if and only if
	$\Set[T]$ and $\Set[T']$ are equivalent.
\end{lemma}

\begin{proof}
	If $\Set[T] \simeq \Set[T']$
	we have
	\[ T[\E] \simeq \Geom(\E, \Set[T])
	\simeq \Geom(\E, \Set[T']) \simeq T'(\E) \]
	natural in $\E$.
	Conversely, if $\phi_\E : T(\E) \xrightarrow{\simeq} T'(\E)$
	is a Morita equivalence,
	we obtain geometric morphisms
	$f : \Set[T] \to \Set[T']$ and $g : \Set[T'] \to \Set[T]$
	corresponding to the models $\phi_{\Set[T]}(U_T) \in T'(\Set[T])$
	and $\phi^{-1}_{\Set[T']}(U_{T'}) \in T(\Set[T'])$.
	Then $f$ and $g$ are quasi-inverses by the various naturalities,
	one half provided by the following diagram.
	\[ \begin{tikzcd}
		\Geom(\Set[T], \Set[T'])
		\ar[r, "\simeq"]
		\ar[d, "\placeholder \circ g"]
		& T'(\Set[T])
		\ar[d, "g^*"]
		& T(\Set[T])
		\ar[l, "\simeq" above, "\phi" below]
		\ar[d, "g^*"] \\
		\Geom(\Set[T'], \Set[T'])
		\ar[r, "\simeq"]
		& T'(\Set[T'])
		& T(\Set[T'])
		\ar[l, "\simeq" above, "\phi" below]
	\end{tikzcd} \]
	This is of course an application of a higher dimensional Yoneda lemma.
\end{proof}

% TODO: state that there is a theory for every topos and conversely?
