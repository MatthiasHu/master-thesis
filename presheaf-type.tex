\section{Theories of presheaf type}

In this section we will introduce the abstract machinery
% TODO: "section"?
which we intend to use later.
The central notion is that of a geometric theory
being \emph{of presheaf type}.
It turns out that theories of presheaf type
allow a very convenient description of a classyfing topos
and also provide access to quotients of the theory.
For all proofs, we refer to
the recent book \cite{theories-sites-toposes}.

\begin{definition}
	A geometric theory $T$ is \emph{of presheaf type}
	if there is a small category $C$
	such that the presheaf topos $[C, \Set]$
	is a classifying topos for $T$.
\end{definition}

\begin{theorem}
	\label{theorem-cartesian-presheaf-type}
	Every cartesian theory is of presheaf type.
\end{theorem}

\begin{proof}
	See \cite[Theorem 2.1.8]{theories-sites-toposes}.
\end{proof}

By Theorem \ref{theorem-cartesian-presheaf-type} it is now clear
that the theories
$\Ring$, $\Alg{K}$, $\AlgQuot{K}$ and $\AlgsAlg{K}{R}$
are of presheaf type, since they are all cartesian.
$\Ring$, $\Alg{K}$ and $\AlgsAlg{K}{R}$ are even algebraic,
while $\AlgQuot{K}$ is a \emph{Horn theory}:
its axioms only use finite conjunctions (including $\top$),
no existential quantification at all.
However, the existential quantifier in the axioms of $\AlgsQuot{K}{R}$
can not be shown to refer to unique existence,
so this is not a cartesian theory.
Only in the special case $R = K$ we have already seen that
the theory gets Morita-equivalent to $\AlgQuot{K}$
and is therefore of presheaf type.
$\AlgNilQuot{K}$ and $\AlgsNilQuot{K}{R}$ even contain
infinitary disjunctions and thus are not cartesian.
We will answer the question
whether they are still of presheaf type later.

The main reason why we are interested in theories of presheaf type is,
that the classifying topos for any such theory
has a very useful \emph{canonical} site of definition,
namely the dual of the category
of finitely presentable models of the theory.
We first recall the notions of
compact object and filtered category.

\begin{definition}
	\label{def-filtered-category}
	A category $C$ is \emph{filtered}
	if it has cocones on all finite diagrams.
	Equivalently, if the following three conditions are satisfied.
	\begin{itemize}
		\item
			There is at least one object $c \in C$.
		\item
			For any two objects $c_1, c_2 \in C$,
			there is an object $c' \in C$
			and some arrows $c_1 \to c'$, $c_2 \to c'$.
		\item
			For any two parallel arrows $f, g : c \to c'$,
			there is an object $c'' \in C$ and an arrow $h : c' \to c''$
			such that $h \circ f = h \circ g$.
	\end{itemize}
	A \emph{filtered colimit} is one such that the index category
	is a filtered small category.
\end{definition}

\begin{definition}
	\label{def-compact-object}
	Let $C$ be a locally small category with all filtered colimits.
	An object $c$ of $C$ is \emph{compact}
	if the corepresentable functor $\Hom(c, \placeholder) : C \to \Set$
	preserves filtered colimits.
	That is, if for every filtered small category $I$
	and functor $F : I \to C$ the canonical map
	\[ \colim_{i \in I} \Hom(c, F(i))
	\to \Hom(c, \colim_{i \in I} F(i)) \]
	is a bijection.
	We will denote the full subcategory
	on the compact objects of $C$ as $C_c$.

	If $C$ happens to be the category $T(\Set)$
	of $\Set$-based models of a geometric theory $T$,
	then the compact objects of $C = T(\Set)$ are called the
	\emph{finitely presentable} models of the theory $T$.
\end{definition}

\begin{theorem}
	\label{theorem-canonical-site}
	Let $T$ be a theory of presheaf type.
	Let $T(\Set)_c$ be the category of its finitely presentable models
	(that is, the full subcategory of $T(\Set)$ on the compact objects).
	Then $[T(\Set)_c, \Set]$ is a classifying topos for $T$.
\end{theorem}

\begin{proof}
	See \cite[Section 6.1.1]{theories-sites-toposes}.
\end{proof}

\begin{remark}
	The category $T(\Set)$ is usually not small,
	but $T(\Set)_c$ can be shown to be essentially small.
	So what we meant by the functor category $[T(\Set)_c, \Set]$
	is to replace $T(\Set)_c$ by a small skeleton first.
\end{remark}

\begin{remark}
	Theorem \ref{theorem-canonical-site} implies that
	the classification of theories up to Morita equivalence can,
	for theories of presheaf type,
	be achieved by computing their compact $\Set$-based models:
	If $T$ and $T'$ are Morita-equivalent,
	of course $T(\Set)_c$ and $T'(\Set)_c$ must be equivalent
	(because compactness is a categorical property).
	The theorem gives the converse implication
	in case $T$ and $T'$ are both of presheaf type.

	In general, the $\Set$-based models
	do not suffice to distinguish geometric theories.
	Indeed, there are geometric theories which have
	no models in $\Set$ at all, but are consistent
	and do have models in other toposes,
	so they are not Morita-equivalent to an inconsistent theory,
	which has no models in any topos.
\end{remark}

The next question after knowing the classifying topos of a theory
is of course to identify the universal model in that topos,
so here we go.

\begin{theorem}
	\label{theorem-canonical-site-universal-model}
	In the situation of Theorem \ref{theorem-canonical-site},
	a universal model of the theory $T$ is given by $N_T$ as follows.
	For a sort $A$,
	$A_{N_T}$ is the functor $T(\Set)_c \to \Set$
	given by $M \mapsto A_M$.
	For a function symbol $f$,
	$f_{N_T}$ is the natural transformation which assigns
	to an object $M \in T(\Set)_c$ the map $f_M$.
	And for a relation Symbol $R \rightarrowtail A_1,\ldots,A_n$,
	$R_{N_T}$ evaluated at $M$ is the subset
	$R_M \subseteq {A_1}_M \times \ldots \times {A_n}_M$.
\end{theorem}

\begin{proof}
	See \cite[Theorem 6.1.1]{theories-sites-toposes}.
\end{proof}

\begin{remark}
	There are weaker (and earlier) versions of
	Theorems \ref{theorem-canonical-site}
	and \ref{theorem-canonical-site-universal-model},
	like \cite[Corollary D3.1.2]{elephant},
	which assumes $T$ to be a cartesian theory.
	While this is sufficient in many cases,
	we will need the theorems in the full generality stated here
	in the last section.
\end{remark}

Note that we deviate from the notation in \cite{theories-sites-toposes},
where the category of finitely presentable models of $T$
is denoted $\mathrm{f.p.}T\text{-}\mathrm{mod}(\Set)$.
This is because we like to emphasize that the notion of compactness,
which distinguishes the finitely presentable models,
is a categorical one and does not depend on the notion of a model.
Also, this reduces the risk of confusion concerning another,
quite similar term, namely that of a \emph{finitely presented} model.
It is a property of theories of presheaf type
that for them the finitely presented models are exactly
the finitely presentable, \ie compact ones,
as we will state
in Theorem \ref{theorem-compact-finitely-presented} below.
For the same reasons
we will also prefer the term \enquote{compact model}
over the term \enquote{finitely presentable model},
especially if it is not clear
whether the theory in question is of presheaf type.

\begin{definition}
	Let $T$ be a geometric theory.
	A $\Set$-based model $M$ of $T$ is \emph{(finitely) presented}
	by a geometric formula $\phi$
	if it corepresents the functor
	\[ T(\Set) \to \Set, \quad N \mapsto \interpretation{\phi}_N \]
	sending a model to the interpretation of $\phi$ in that model.
\end{definition}

This can be spelled out as follows.
Let $x_1 : A_1, \ldots, x_n : A_n$ be the context
of the formula $\phi$.
Then an element of $\interpretation{\phi}_N$ is given by
elements $a_1 \in {A_1}_N, \ldots, a_n \in {A_n}_N$
such that $\phi$ holds for $a_1, \ldots, a_n$.
So $M$ is presented by $\phi$ if and only if
there are elements $a_i \in {A_i}_M$ (the \emph{generators} of M)
such that $\phi$ holds for the $a_i$ and,
for any model $N \in T(\Set)$ and elements $b_i \in {A_i}_N$
for which $\phi$ holds,
there is a unique homomorphism $f : M \to N$
such that $f(a_i) = b_i$.
% TODO: is "holds" clear?

\begin{theorem}
	\label{theorem-compact-finitely-presented}
	Let $T$ be a theory of presheaf type
	and $M$ a $\Set$-based model of~$T$.
	Then $M$ is compact as an object of $T(\Set)$ if and only if
	$M$ is presented by some geometric formula.
\end{theorem}

\begin{proof}
	See \cite[Corollary 6.1.15]{theories-sites-toposes}.
\end{proof}

The final tool to be introduced here
concernes quotients of a theory of presheaf type.
If $T'$ is a quotient of a geometric theory $T$,
the category $T'(\E)$ of models of $T'$
is a full subcategory of $T(\E)$
for any Grothendieck topos $\E$.
In terms of classifying toposes
this means that $\Geom(\E, \Set[T'])$
must be a full subcategory of $\Geom(\E, \Set[T])$.
This is the case if $\Set[T']$ is a subtopos of $\Set[T]$,
and indeed, there is a direct correspondence
between quotients of a geometric theory $T$
and subtoposes of $\Set[T]$.
For the details see \cite[Theorem 3.2.5]{theories-sites-toposes}.

Now, if we have a Grothendieck topos $\Sh(C, J)$,
the subtoposes of $\Sh(C, J)$ correspond to
those Grothendieck topologies on $C$ which contain $J$
(and therefore \enquote{select}
a smaller full subcategory of $[\op{C}, \Set]$ than $\Sh(C, J)$).
So we would like to know, for a theory $T$ of presheaf type,
how to describe the Gothendieck topology on $\op{T(\Set)_c}$
corresponding to a quotient of the theory $T$.

For this purpose, we need to explain what it means for a formula
to present a homomorphism of models (instead of a model).
Let $\phi$ and $\psi$ be formulas
presenting models $M_\phi, M_\psi \in T(\Set)_c$.
Let furthermore $\theta$ be a formula
in the context $\vec{x}, \vec{y}$
where $\vec{x}$ (respectively $\vec{y}$)
is the context of $\phi$ (respectively $\psi$).
Assume that $\theta$ is \emph{provably functional}
from $\psi$ to $\phi$,
meaning that the following sequents are provable in $T$.
\begin{gather*}
	\theta \turnstile{\vec{y}, \vec{x}}
	\phi \land \psi \\
	\psi \turnstile{\vec{y}}
	\exx{\vec{x}} \theta \\
	\theta \land \theta[\vec{\tilde{x}}/\vec{x}]
	\turnstile{\vec{y}, \vec{x}, \vec{\tilde{x}}}
	\vec{x} = \vec{\tilde{x}}
\end{gather*}
Then for any model $N \in T(\Set)$,
the interpretation $\interpretation{\theta}_N \subseteq
\interpretation{\psi}_N \times \interpretation{\phi}_N$
is the graph of a map
$\interpretation{\psi}_N \to \interpretation{\phi}_N$.
In particular, if $\vec{a} \in \interpretation{\psi}_{M_\psi}$
is the tuple of generators of $M_\psi$,
then there is a unique element
$\vec{b} \in \interpretation{\phi}_{M_\psi}$
with $(\vec{a}, \vec{b}) \in \interpretation{\theta}_{M_\psi}$.
And since $\phi$ presents $M_\phi$,
there is a unique model homomorphism $s_\theta : M_\phi \to M_\psi$
sending the generators of $M_\phi$ to $\vec{b}$.
This arrow $s_\theta$ is the arrow \emph{presented}
by the provably functional formula $\theta$.

\begin{theorem}
	\label{theorem-canonical-site-topology}
	Let $T$ be a theory of presheaf type.
	Let $\phi_i$, $i \in I$ be geometric formulas
	(in contexts $\vec{x}_i$)
	presenting models $M_i \in T(\Set)_c$.
	For each $i \in I$,
	let $\psi_i^j, \theta_i^j$, $j \in J_i$ be geometric formulas,
	where each $\psi_i^j$ (in context $\vec{y}_i^j$)
	presents a model $M_i^j \in T(\Set)_c$
	and each $\theta_i^j$ is provably functional
	from $\psi_i^j$ to $\phi_i$.
	Finally, let $T'$ be the quotient of $T$ with the additional axioms
	\[ \phi_i \turnstile{\vec{x}_i}
	\bigvee_{j \in J_i} \exx{\vec{y}_i^j} \theta_i^j,
	\qquad i \in I. \]
	Then the Grothendieck topology on $\op{T(\Set)_c}$ induced by $T'$
	is generated by the sieves $S_i$, $i \in I$,
	where $S_i$ is the dual of
	the cosieve on $M_i$ generated by the arrows $s_i^j : M_i \to M_i^j$
	presented by $\theta_i^j$.
\end{theorem}

\begin{proof}
	See \cite[Theorem 8.1.10]{theories-sites-toposes}.
\end{proof}
