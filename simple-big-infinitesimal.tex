\section{The simple big infinitesimal topos}

We first give the general definition
and reduce to an algebraic situation
before explaining the simplifying assumption
to be used in this section.

\begin{definition}
	\label{definition-big-infinitesimal}
	Let $X \to S$ be schemes.
	The \emph{big infinitesimal topos} of~$X$ over~$S$
	is the topos $X/S_\INF \defeq \Sh(\INF(X/S))$,
	where $\INF(X/S)$ is the following site.
	\begin{itemize}
		\item
			Objects:
			Pairs of schemes $T \to S$, $U \to X$
			locally of finite presentation over $S$ respectively $X$,
			together with a closed immersion $U \to T$ over $S$ such that
			the corresponding quasi-coherent ideal sheaf $I \subseteq \O_T$
			is a sheaf of nil ideals
			(\ie every section on every open subset is nilpotent).
			\[ \begin{tikzcd}
				T \ar[d] & U \ar[l] \ar[d] \\
				S & X \ar[l]
			\end{tikzcd} \]
		\item
			Morphisms:
			Pairs of morphisms $T' \to T$, $U' \to U$
			compatible with the other data.
			\[ \begin{tikzcd}[column sep=small, row sep=small]
				T' \ar[dd] \ar[dr] & & U' \ar[ll] \ar[dr] \\
				& S & & X \ar[ll] \\
				T \ar[ur] & & U \ar[ll] \ar[ur]
				\ar[from=uu, crossing over]
			\end{tikzcd} \]
		\item
			Grothendieck topology:
			A sieve on $T \leftarrow U$ is $J_{\INF(X/S)}$-covering
			if and only if it contains the arrows
			\[ \begin{tikzcd}
				T_i \ar[d, hook] & U \times_T T_i \ar[d, hook] \ar[l] \\
				T & U \ar[l]
			\end{tikzcd} \]
			for some open covering $T_i \hookrightarrow T$.
	\end{itemize}
\end{definition}

This definition is in analogy to the little infinitesimal topos
as defined in \cite[Section 4.1.]{dix-exposes-ix}.
Note that $T_i \leftarrow U \times_T T_i$ is indeed again
a closed immersion corresponding to a sheaf of nil ideals
(namely the restriction of the sheaf corresponding to $U$
to the open set $T_i$).

% TODO: check Grothendieck topology axioms

% TODO: homeomorphism because of nil ideal

\begin{lemma}
	The full subcategory of $\INF(X/S)$
	on the objects $T \leftarrow U$
	with both $T$ and $U$ affine
	is dense.
\end{lemma}

\begin{proof}
	For any object $T \leftarrow U$,
	we can cover $T$ by affine opens $T_i \hookrightarrow T$.
	Then the pullbacks $U \times_T T_i$ are also affine,
	since $U \to T$ is a closed immersion.
\end{proof}

We denote this dense full subcategory by $\INF^*(X/S)$.

\begin{lemma}
	\label{lemma-INF-localization-homomorphisms}
	Let $J_{\INF^*(X/S)} = J_{\INF(X/S)}|_{\INF^*(X/S)}$
	be the restriction of $J_{\INF(X/S)}$ to $\INF^*(X/S)$
	as in the Comparison Lemma (Theorem \ref{theorem-comparison-lemma}).
	Then a sieve $\tilde{S}$ on $T = \Spec A \leftarrow U = \Spec B$
	(given by $f : A \to B$)
	is $J_{\INF^*(X/S)}$-covering if and only if
	there are elements $a_1, \ldots, a_k \in A$
	such that $\sum_{i=1}^k a_i = 1$
	and all the morphisms given by
	\[ \begin{tikzcd}
		A \ar[d] \ar[r, two heads, "f"] & B \ar[d] \\
		A[a_i^{-1}] \ar[r, two heads] & B[f(a_i)^{-1}]
	\end{tikzcd} \]
	for $i = 1, \ldots, k$ lie in $\tilde{S}$.
\end{lemma}

\begin{proof}
	The dual of the displayed family
	generates a $J_{\INF^*(X/S)}$-covering sieve,
	because the $\Spec A[a_i^{-1}] \to \Spec A$ are an open covering
	and $\Spec B[f(a_i)^{-1}]$ is the pushout as in Definition
	\ref{definition-big-infinitesimal}.
	Now let $\tilde{S}$ be a sieve in $\ZAR^*(X/S)$ which generates
	a covering sieve in $\ZAR(X/S)$.
	Then the generated sieve contains arrows
	$(T_i \leftarrow U_i)
	\to (\Spec A \leftarrow \Spec B)$,
	where $U_i = U \times_{\Spec A} T_i$,
	such that $T_i \to \Spec A$ is an open covering.
	This can be chosen so that the $T_i$ are affine,
	\ie of the above form $\Spec A[a_i^{-1}] \to \Spec A$.
	Then the $U_i$ are affine too,
	namely $U_i \cong \Spec B[f(a_i)^{-1}]$,
	so the dual of a family as displayed in the statement
	was contained in $\tilde{S}$.
\end{proof}

\begin{lemma}
	\label{lemma-inf-algebraic-site}
	If $S = \Spec K$, $X = \Spec R$ are both affine,
	then the category $\INF^*(\Spec R/\Spec K)$ is equivalent to
	the full subcategory of $\op{\AlgsNilQuot{K}{R}(\Set)}$
	on those objects $A \twoheadrightarrow B$
	such that $A$ is a finitely presented $K$-algebra
	and $B$ is a finitely presented $R$-algebra.
\end{lemma}

\begin{proof}
	Of course we want to map such an $f : A \twoheadrightarrow B$
	to $\Spec A \leftarrow \Spec B$.
	That $A$ (respectively $B$) is
	a finitely presented $K$-algebra (respectively $R$-algebra)
	is the same as to say that
	$\Spec A$ (respectively $\Spec B$)
	is locally of finite presentation
	over $\Spec K$ (respectively $\Spec R$).
	The homomorphism $f$ being surjective
	corresponds to $\Spec A \leftarrow \Spec B$ being a closed immersion,
	and the kernel of $f$ is a nil ideal
	if and only if the corresponding ideal sheaf on $\Spec A$
	is a sheaf of nil ideals.
\end{proof}

To make use of Lemma \ref{lemma-inf-algebraic-site},
we need to prove that $\AlgsNilQuot{K}{R}$ is of presheaf type
and show that said subcategory is $\op{\AlgsNilQuot{K}{R}(\Set)_c}$.
This is the point where we temporarily restrict our attention
to a special case, namely that of $R = K$.

\begin{definition}
	Let $S$ be a scheme.
	We call $S_\INF \defeq (S/S)_\INF = \Sh(\INF(S))$
	with $\INF(S) \defeq \INF(S/S)$
	the \emph{simple big infinitesimal topos}.
\end{definition}

We have seen in Corollary \ref{corollary-K-Alg-K-NilQuot-K-AlgNilQuot}
that $\AlgsNilQuot{K}{K}$ is Morita-equivalent to $\AlgNilQuot{K}$,
and identified the compact models in
Proposition \ref{proposition-compact-K-AlgNilQuot}
to be those described in Lemma \ref{lemma-inf-algebraic-site}.
It is still not obvious whether $\AlgNilQuot{K}$ is of presheaf type,
since it contains the following nilpotence axiom,
which prevents it from being a cartesian theory.
\[ x \in \mathfrak{a} \turnstile{x : A}
\bigvee_{n \in \NN_{\geq 0}} x^n = 0 \]
We will now further reformulate this theory
to a Morita-equivalent theory which is cartesian.

If we had an upper bound $N$ on the nilpotence index
of (generalized) elements of any model,
we could simply replace the infinitary disjunction by $x^N = 0$.
This is of course not possible, as there are models (already in $\Set$)
with elements of arbitrarily high nilpotence index.
Instead of deciding for one of the formulas $x^n = 0$ for a fixed $n$,
we will impose them all, but under different premises.

Let $A$ be a ring (in $\Set$) and $N \subseteq A$ an arbitrary subset
containing only nilpotent elements.
Then we can \enquote{stratify} $N$ into the subsets
$N_n \defeq \{ a \in N \mid a^n = 0 \}$,
for each of which there is a bound $n$
on the nilpotence index of its elements.
To make this stratification unique,
we must additionally require $N_n = \{a \in N_{n+1} \mid a^n = 0\}$.
Finally, can we formulate the requirement
that $N = \bigcup N_n$ should be an ideal,
only refering to the $N_n$ but not to their union?
This is possible,
since the nilpotence index of elements can not increase arbitrarily
under the operations needed to define an ideal.

\begin{proposition}
	\label{proposition-stratified-nil-ideal}
	The theory $\AlgNilQuot{K}$ is Morita-equivalent
	to the theory obtained from $\Alg{K}$ by adding
	unary relation symbols $\mathfrak{a}_n \rightarrowtail A$
	for $n \in \NN_{\geq 0}$
	and the following axioms
	(for each $n, m \in \NN_{\geq 0}$ wherever occuring).
	\begin{gather*}
		x \in \mathfrak{a}_n \doubleturnstile{x : A}
		(x^n = 0) \land (x \in \mathfrak{a}_{n+1}) \\
		\top \turnstile{[]} 0 \in \mathfrak{a}_1 \\
		x \in \mathfrak{a}_n \turnstile{x, y : A}
		x \cdot y \in \mathfrak{a}_n \\
		(x \in \mathfrak{a}_n) \land (y \in \mathfrak{a}_m)
		\turnstile{x, y : A} x + y \in \mathfrak{a}_{n + m -1}
	\end{gather*}
\end{proposition}

\begin{proof}
	The conversion between models of the two theories
	is relatively simple here,
	because the signatures only differ in relation symbols
	($\mathfrak{a}$ for one, $\mathfrak{a}_n$, $n \geq 0$ for the other).
	Given a model $M$ of $\AlgNilQuot{K}$ in any Grothendieck topos,
	we define subobjects ${\mathfrak{a}_n}_M \defeq
	\interpretation{(x \in \mathfrak{a}) \land (x^n = 0)}_M
	\hookrightarrow A_M$.
	We have obtained a model of the second theory,
	since all of the above axioms are provable in $\AlgNilQuot{K}$
	when every application $x \in \mathfrak{a}_n$
	of one of the relation symbols not present in $\AlgNilQuot{K}$
	is replaced by the formula $(x \in \mathfrak{a}) \land (x^n = 0)$.
	(Here one calculates, within the theory,
	that $(xy)^n = 0$ for $x^n = 0$,
	and $(x + y)^{n + m - 1} = 0$ for $x^n = 0$, $y^m = 0$.)
	Next, let instead ${\mathfrak{a}_n}_M \hookrightarrow A_M$
	be subobjects such that the above axioms are valid.
	Then we set $\mathfrak{a}_M \defeq
	\interpretation{\bigvee_{n \geq 0} x \in \mathfrak{a}_n}
	\hookrightarrow A_M$,
	that is, $\mathfrak{a}_M$ is the union
	of the subobjects ${\mathfrak{a}_n}_M$.
	And this time, we have to check that $\mathfrak{a}_M$
	fulfills the axioms for an ideal,
	which is easily done.

	To see that these constructions are inverses of each other
	(up to isomorphism,
	\ie different representations of the same subobjects),
	we only have to prove the following sequents,
	which follow readily from the respective axioms.
	\begin{gather*}
		x \in \mathfrak{a} \doubleturnstile{x : A}
		\bigvee_{n \geq 0} ((x \in \mathfrak{a}) \land (x^n = 0)) \\
		x \in \mathfrak{a}_n \doubleturnstile{x : A}
		(x^n = 0) \land \bigvee_{n \geq 0} x \in \mathfrak{a}_n
	\end{gather*}

	There is no additional restriction imposed on homomorphisms of models
	by the relation symbols $\mathfrak{a}_n$,
	since $(f(x))^n = 0$ follows from $x^n = 0$ if $f$ satisfies
	the ring homomorphism axioms.
	(Or alternatively, since model homomorphisms respect
	the interpretation of geometric formulas.)
	So the established correspondence between models
	is indeed an equivalence of categories.
	Finally, naturality in the topos argument
	is given because geometric morphisms
	preserve the interpretation of geometric formulas.
\end{proof}

\begin{corollary}
	\label{corollary-K-AlgNilQuot-presheaf-type}
	The theory $\AlgNilQuot{K}$ is of presheaf type,
	and therefore classified by $\PSh(\INF^*(\Spec K))$.
\end{corollary}

\begin{proof}
	The theory given in Proposition \ref{proposition-stratified-nil-ideal}
	is a cartesian theory (in fact, it is a Horn theory),
	so it is of presheaf type
	(by Theorem \ref{theorem-cartesian-presheaf-type}),
	and so is $\AlgNilQuot{K}$.
	Theorem \ref{theorem-canonical-site}
	and Lemma \ref{lemma-inf-algebraic-site}
	provide the second part of the statement.
\end{proof}

\begin{lemma}
	\label{lemma-topology-INF-generators}
	The Grothendieck topology $J_{\INF^*(\Spec K)}$
	on $\INF^*(\Spec K) \simeq \op{\AlgNilQuot{K}(\Set)_c}$
	is generated by the duals of the following families
	for $n \in \NN_{\geq 0}$.
	\[ \begin{tikzcd}
		(0) \triangleleft K[X_1, \ldots, X_n]/(X_1+\ldots+X_n-1)
		\ar[d] \\
		(0) \triangleleft K[X_1, \ldots, X_n, X_i^{-1}]/(X_1+\ldots+X_n-1)
	\end{tikzcd}
	\qquad i = 1,\ldots,n \]
\end{lemma}

\begin{proof}
	For $X = S$, the families displayed in
	Lemma \ref{lemma-INF-localization-homomorphisms}
	can be written as
	\[ \begin{tikzcd}
		\mathfrak{a} \triangleleft A \ar[d] \\
		\mathfrak{a}[a_i^{-1}] \triangleleft A[a_i^{-1}]
	\end{tikzcd}
	\qquad i = 1,\ldots, n \]
	with $\mathfrak{a} \defeq \ker(A \twoheadrightarrow B)$,
	because $B$ carries no extra structure
	than being a quotient of $A$ by a nil ideal.
	The families in the statement are clearly special cases of this,
	so they do generate $J_{\INF^*(\Spec K)}$-covering sieves.
	And by pushout along
	$((0) \triangleleft K[X_1, \ldots, X_n]/(X_1 + \ldots + X_n - 1))
	\to (\mathfrak{a} \triangleleft A), X_j \mapsto a_j$
	we can again recover all covering sieves from these special cases.
\end{proof}

\begin{theorem}
	The simple big infinitesimal topos $(\Spec K)_\INF$
	classifies the theory $\loc\AlgNilQuot{K}$.
\end{theorem}

\begin{proof}
	Just as for the big Zariski topos,
	we use Theorem \ref{theorem-canonical-site-topology}
	to describe the topology on $\op{\AlgNilQuot{K}(\Set)_c}$
	corresponding to the locality axioms.
	Indeed, we can take the same formulas as before,
	and only have to find new models presented by them,
	this time in $\AlgNilQuot{K}(\Set)$ instead of $\Alg{K}(\Set)$:
	$\phi_n \defequiv (x_1 + \ldots + x_n = 1)$,
	$\psi_n^i \defequiv (\tilde{x}_1 + \ldots + \tilde{x}_n = 1)
	\land (\tilde{x}_i \cdot y = 1)$
	and $\theta_n^i \defequiv \psi_n^i \land
	(x_1 = \tilde{x}_1) \land \ldots \land (x_n = \tilde{x}_n)$.

	One can check that $\phi_n$ and $\psi_n^i$ present precisely
	the models they presented in $\Alg{K}(\Set)$,
	with $\mathfrak{a}$ set to the zero ideal.
	\begin{gather*}
		M_{\phi_n} = ((0) \triangleleft
		K[X_1, \ldots, X_n]/(X_1+\ldots+X_n-1)) \\
		M_{\psi_n^i} = ((0) \triangleleft
		K[X_1, \dots, X_n,X_i^{-1}]/(X_1 + \ldots + X_n - 1))
	\end{gather*}
	This is because of the adjunction
	\[ \Hom_{\AlgNilQuot{K}(\Set)}((0) \triangleleft A,
	\mathfrak{a}' \triangleleft A')
	\cong \Hom_{\Alg{K}(\Set)}(A, A') \]
	we have already met in
	Proposition \ref{proposition-compact-K-AlgNilQuot}:
	The interpretation of a formula of $\AlgNilQuot{K}$,
	which does not contain the relation symbol $\mathfrak{a}$,
	is, as a functor $\AlgNilQuot{K}(\Set) \to \Set$,
	the composition of the interpretation of the same formula
	read as a formula of $\Alg{K}$
	and the forgetful functor $\AlgNilQuot{K}(\Set) \to \Alg{K}(\Set),
	(\mathfrak{a} \triangleleft A) \mapsto A$.
	One can also check that $\theta_n^i$ still presents
	the canonical localization homomorphism,
	so we have obtained, as generating families
	for the topology corresponding to the locality axioms,
	exactly the families from Lemma~\ref{lemma-topology-INF-generators}.
\end{proof}
