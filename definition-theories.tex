\section{Definition of the relevant theories}

\begin{definition}
	The \emph{theory of rings}, denoted $\Ring$, consists of
	a single sort $A$,
	two constant symbols $0, 1 : A$,
	two binary function symbols $+, \cdot : A,A \to A$,
	one unary function symbol $- : A \to A$
	and axioms expressing that
	$(A, +, 0, -)$ is an abelian group
	\begin{gather*}
		\top \turnstile{x : A} 0 + x = x, \qquad
		\top \turnstile{x, y : A} x + y = y + x, \\
		\top \turnstile{x, y, z : A} (x + y) + z = x + (y + z), \qquad
		\top \turnstile{x : A} x + (-x) = 0,
	\end{gather*}
	$(A, \cdot, 1)$ is a commutative monoid
	\begin{gather*}
		\top \turnstile{x : A} 1 \cdot x = x, \qquad
		\top \turnstile{x, y : A} x \cdot y = y \cdot x, \\
		\top \turnstile{x, y, z : A}
		(x \cdot y) \cdot z = x \cdot (y \cdot z),
	\end{gather*}
	and multiplication is distributive over addition
	\[ \top \turnstile{x, y, z : A}
	x \cdot (y + z) = (x \cdot y) + (x \cdot z). \]
\end{definition}

\begin{remark}
	We could leave out the function symbol for negation $- : A \to A$
	and interpret \enquote{abelian group} to mean the axiom
	\[ \top \turnstile{x : A} \ex{y}{A} x + y = 0 . \]
	The existential quantifier here can easily be shown
	to refer to a unique existence,
	so this theory is not algebraic but it is still a cartesian theory.
	It is indeed Morita-equivalent to the theory given above:
	If we have a function symbol for negation,
	the axiom asserting the existence of negatives of course holds.
	So we only have to show from the existence axiom that
	there is a unique morphism $- : A \to A$ fulfilling
	$\top \turnstile{x : A} x + (-x) = 0$.
	For this, we observe that the formula $x + y = 0$
	is provably functional
	(from the context $x : A$ to the context $y : A$),
	meaning that in addition to the existence axiom
	we can also prove (using the other axioms of the theory of rings)
	\[ x + y = 0 \land x + y' = 0 \turnstile{x, y, y' : A} y = y'. \]
	From this we can conclude by \cite[Proposition D1.3.12]{elephant}
	that there indeed exists a unique morphism $- : A \to A$ as above.
	One can also check that negation is respected by
	any homomorphism of models of the theory without negation symbol,
	completing the Morita equivalence.

	However, we prefer to think about the theory of rings
	as an algebraic theory
	and therefore included the function symbol for negation.
	In \cite[Section X.3]{maclane-moerdijk}
	this issue seems to have been overlooked,
	as it is stated that the theory of rings
	could be formulated with only $0, 1, +, \cdot$
	and algebraic axioms.
\end{remark}

\begin{definition}
	Let $K$ be a ring.
	The \emph{theory of $K$-algebras}, denoted $\Alg{K}$,
	consists of one sort $A$,
	all the function symbols (including constant symbols) and axioms of
	the theory of rings
	and additionally one constant symbol $c_\lambda : A$
	for every element $\lambda \in K$,
	together with the axioms
	\begin{gather*}
		\top \turnstile{[]} c_0 = 0, \qquad
		\top \turnstile{[]} c_1 = 1, \\
		\top \turnstile{[]} c_\lambda + c_\mu = c_{\lambda+\mu}, \qquad
		\top \turnstile{[]} c_\lambda \cdot c_\mu = c_{\lambda\cdot\mu},
	\end{gather*}
	the latter two for all $\lambda, \mu \in K$,
	expressing the properties of a ring homomorphism from $K$ to $A$.
\end{definition}

\begin{definition}
	Let $K$ be a ring.
	\begin{itemize}
		\item
			The theory $\AlgQuot{K}$
			of \emph{$K$-algebras with an ideal}
			consists of one sort $A$,
			all function symbols and axioms of $\Alg{K}$,
			one unary relation symbol $\mathfrak{a} \rightarrowtail A$
			and axioms expressing that $\mathfrak{a}$ is an ideal
			(we denote the \enquote{application} of $\mathfrak{a}$
			to a term $t$ as $t \in \mathfrak{a}$)
			\begin{gather*}
				\top \turnstile{[]} 0 \in \mathfrak{a} \\
				x \in \mathfrak{a} \land y \in \mathfrak{a}
				\turnstile{x, y : A} x + y \in \mathfrak{a} \\
				x \in \mathfrak{a} \turnstile{x, y : A}
				x \cdot y \in \mathfrak{a}.
			\end{gather*}
		\item
			The theory $\AlgNilQuot{K}$
			of \emph{$K$-algebras with a nil ideal}
			is the quotient of $\AlgQuot{K}$
			with one additional axiom
			\[ x \in \mathfrak{a} \turnstile{x : A}
			\bigvee_{n \in \NN_{\geq 0}} x^n = 0. \]
			Here, $x^n$ of course stands for the term
			$(\ldots ((1 \cdot x) \cdot x) \ldots) \cdot x$
			with $x$ occuring $n$ times.
	\end{itemize}
\end{definition}

An alternative way to think about a $K$-algebra $A$
together with an ideal
is a surjective $K$-algebra homomorphism
from $A$ to some other $K$-algebra
(therefore the name $\AlgQuot{K}$).
We will see in
Proposition \ref{proposition-K-Alg-K-Quot-K-AlgQuot} below
that this indeed leads to a Morita-equivalent theory.
But first we give definitions of such theories
involving a $K$-algebra homomorphism,
generalized by additional structure on the codomain.

\begin{definition}
	Let $K$ be a ring and $R$ be a $K$-algebra.
	\begin{itemize}
		\item
			The theory $\AlgsAlg{K}{R}$
			of \emph{$K$-algebra homomorphisms into an $R$-algebra}
			consists of two sorts $A$ and $B$,
			all function symbols and axioms of $\Alg{K}$ for the sort $A$,
			all function symbols and axioms of $\Alg{R}$ for the sort $B$
			(we will denote the two distinct funtions symbols
			$+ : A, A \to A$ and $+ : B, B \to B$ of the theory
			in the same way,
			and similarly for all other funtion symbols mentioned so far),
			one additional function symbol $f : A \to B$
			and axioms expressing that $f$ is a $K$-algebra homomorphism
			\begin{gather*}
				\top \turnstile{x, y : A} f(x + y) = f(x) + f(y), \qquad
				\top \turnstile{x, y : A} f(x \cdot y) = f(x) \cdot f(y), \\
				\top \turnstile{[]} f(c_\lambda) = c_\lambda,
			\end{gather*}
			the last one for every $\lambda \in K$
			(and reading $\lambda$ as an element of $R$
			on the right side of the equation,
			such that $c_\lambda : B$).
		\item
			The theory $\AlgsQuot{K}{R}$
			of \emph{surjective $K$-algebra homomorphisms into an $R$-algebra}
			is the quotient of $\AlgsAlg{K}{R}$
			with the additional axiom
			\[ \top \turnstile{y : B} \ex{x}{A} f(x) = y. \]
		\item
			The theory $\AlgsNilQuot{K}{R}$
			of \emph{surjective $K$-algebra homomorphisms
			into an $R$-algebra with nil kernel}
			is the quotient of $\AlgsQuot{K}{R}$
			with the additional axiom
			\[ f(x) = 0 \turnstile{x : A}
			\bigvee_{n \in \NN_{\geq 0}} x^n = 0. \]
	\end{itemize}
\end{definition}

\begin{proposition}
	\label{proposition-K-Alg-K-Quot-K-AlgQuot}
	The theories $\AlgsQuot{K}{K}$ (surjective $K$-algebra homomorphisms)
	and $\AlgQuot{K}$ ($K$-algebras with an ideal)
	are Morita-equivalent.
\end{proposition}

\begin{proof}
	Let $\E$ be a Grothendieck topos.
	We give explicit constructions
	to convert models of $\AlgsQuot{K}{K}$ and $\AlgQuot{K}$
	into each other,
	\ie for the equivalence
	\[ \AlgsQuot{K}{K}(\E) \simeq \AlgQuot{K}(\E). \]

	First let $(f : A \twoheadrightarrow B) \in \AlgsQuot{K}{K}(\E)$.
	Then we take as subobject $\mathfrak{a}$ of $A$
	the interpretation of the formula $f(x) = 0$
	(in the context $x : A$),
	that is, the equalizer
	\[ \begin{tikzcd}
		\mathfrak{a} \ar[r, hook]
		& A \ar[r, two heads, "f", shift left]
		\ar[r, "0" below, shift right]
		& B
	\end{tikzcd} \]
	(where the arrow annotated $0$ is the composite
	$A \to 1 \xrightarrow{0} B$ over the terminal object $1$).
	It is easy to show that the predicate $f(x) = 0$
	fulfills the axioms for an ideal,
	so we have obtained
	a model $\mathfrak{a} \triangleleft A$ of $\AlgQuot{K}$.

	If $\mathfrak{a} \triangleleft A$ is a given model of $\AlgQuot{K}$,
	we have to construct an epimorphism $f : A \twoheadrightarrow B$
	(because the surjectivity axiom
	translates exactly to being an epimorphism)
	and also provide a $K$-algebra structure on $B$.
	For $f$ we take the coequalizer
	\[ \begin{tikzcd}
		\mathfrak{a} \times A
		\ar[r, "+", shift left]
		\ar[r, "\pi_2" below, shift right]
		& A
		\ar[r, "f", two heads]
		& B.
	\end{tikzcd} \]
	The constants $c_\lambda$ for $\lambda \in K$
	are easily defined for $B$, we just have to compose with~$f$.
	For the other function symbols like $+ : B \times B \to B$,
	a bit more work is needed.
	One has to show that $f \circ {+} : A \times A \to B$
	factors over $f \times f : A \times A \to B \times B$,
	mimicking the usual algebraic calculations needed
	to prove the ring operations on a quotient ring well-defined.
	The axioms for the $K$-algebra structure on $B$
	follow then immediately from the same axioms for $A$
	and the epimorphism $f$ commuting with the various operations.
	For example, the axiom $0 + x = x$ means
	that the horizontal composites in the diagram below
	are the identity on $A$ respectively $B$.
	\[ \begin{tikzcd}
		A \ar[r, "{(0, \id_A)}"] \ar[d, two heads, "f"]
		& A \times A \ar[r, "+"] \ar[d, two heads, "f \times f"]
		& A \ar[d, two heads, "f"] \\
		B \ar[r, "{(0, \id_B)}"]
		& B \times B \ar[r, "+"]
		& B
	\end{tikzcd} \]

	Having defined the two parts of the equivalence on objects,
	one checks that for two such \enquote{extended models}
	$\mathfrak{a} \triangleleft A \twoheadrightarrow B$
	and $\mathfrak{a}' \triangleleft A' \twoheadrightarrow B'$,
	a $K$-algebra homomorphism $A \to A'$ indeed
	allows a compatible map $B \to B'$
	if and only if it sends $\mathfrak{a}$ into $\mathfrak{a}'$,
	that is, allows a compatible map $\mathfrak{a} \to \mathfrak{a}'$.
	(Here one can reason internally again,
	only giving a formula for the graph of the desired map,
	and then apply \cite[Proposition D1.3.12]{elephant}
	to obtain an actual morphism.)
	Finally, the above constructions are natural in $\E$, that is,
	they are preserved by the inverse image parts of geometric morphisms,
	as we have only used finite limits and colimits.
\end{proof}

\begin{corollary}
	\label{corollary-K-Alg-K-NilQuot-K-AlgNilQuot}
	The theories $\AlgsNilQuot{K}{K}$ and $\AlgNilQuot{K}$
	are Morita-equivalent.
\end{corollary}

\begin{proof}
	These theories differ from $\AlgsQuot{K}{K}$,
	respectively $\AlgQuot{K}$,
	by a nilpotence axiom,
	where the premise is $f(x) = 0$ in the first case
	and $x \in \mathfrak{a}$ in the second.
	But the subobjects defined by these formulas
	correspond to each other under the constructions in the proof of
	Proposition \ref{proposition-K-Alg-K-Quot-K-AlgQuot}.
	So the two nilpotence axioms define \enquote{the same}
	full subcategory of $\AlgsQuot{K}{K}(\E) \cong \AlgQuot{K}(\E)$.
\end{proof}

\begin{definition}
	\label{definition-local-rings}
	\begin{itemize}
		\item
			The \emph{theory of local rings}
			is the quotient of the theory of rings
			with the additional axioms
			\[ x_1 + \ldots + x_n = 1 \turnstile{x_1,\ldots,x_n : A}
			\bigvee_{i=1,\ldots,n} \ex{y}{A} x_i \cdot y = 1 \]
			for all $n \in \NN_{\geq 0}$.
		\item
			The above locality axioms can be added
			to any of the theories defined earlier
			as they all contain a sort $A$ with a ring structure.
			Thus we obtain in particular $\loc\Alg{K}$
			(the \emph{theory of local $K$-algebras}),
			$\loc\AlgNilQuot{K}$ and $\loc\AlgsNilQuot{K}{R}$.
	\end{itemize}
\end{definition}

\begin{remark}
	The (countably many) locality axioms
	given in Definition \ref{definition-local-rings}
	can be replaced by two axioms
	\[ 0 = 1 \turnstile{[]} \bot, \qquad
	x_1 + x_2 = 1 \turnstile{x_1, x_2 : A}
	(\ex{y}{A} x_1 \cdot y = 1) \lor (\ex{y}{A} x_2 \cdot y = 1). \]
	Indeed, these are special cases of the axioms from
	Definition \ref{definition-local-rings} for $n = 0$ and $n = 2$.
	The case $n = 1$ is trivially fulfilled
	(\ie provable from the theory of rings)
	and all other instances can be seen to follow from these
	by induction.
	% TODO: more?
\end{remark}

\begin{remark}
	In a context of classical logic,
	local rings are usually defined as those with a unique maximal ideal.
	Using the Axiom of Choice this is equivalent to our definition:
	Our axioms say that the non-invertible elements form an ideal
	(as they are in any case closed under
	multiplication with arbitrary ring elements),
	which is then of course maximal.
	Conversely, any non-invertible element is
	contained in some maximal ideal by Zorn's lemma,
	so if there is only one maximal ideal
	it must contain precisely all non-invertibles.
\end{remark}

For the theories with two ring structures
we could also (or instead) have added locality axioms for $B$.
However, this would have been redundant (or equivalent)
in the case of $\AlgsNilQuot{K}{R}$
as we can conclude from the following lemma.

\begin{lemma}
	Let $f : A \twoheadrightarrow B$ be a surjective ring homomorphism
	with kernel a nil ideal $\mathfrak{a} \triangleleft A$.
	Then $A$ is a local ring if and only if $B$ is a local ring.
	And this holds intuitionistically
	(with \enquote{local} understood as
	in Definition \ref{definition-local-rings}),
	so it holds in any geometric theory
	with an appropriate ring homomorphism.
\end{lemma}

\begin{proof}
	Let $A$ be local and $b_1 + \ldots + b_n = 1$ in $B$.
	Then there are preimages $a_1, \ldots a_n \in A$
	and $a_1 + \ldots + a_n + \epsilon = 1$
	for some $\epsilon \in \mathfrak{a}$.
	Since $A$ is local,
	one of the $a_i$ or $\epsilon$ must be invertible.
	But $\epsilon$ is nilpotent, so if it is invertible,
	$0 = 1$ holds in $A$,
	which by the locality axiom for $n = 0$ entails $\bot$.

	Conversely, let $B$ be local and $a_1 + \ldots + a_n = 1$ in $A$.
	Then $f(a_1) + \ldots + f(a_n) = 1$ in $B$ and by locality of $B$,
	some $f(a_i)$ is invertible, $f(a_i) \cdot s = 1$.
	For $t \in A$ with $f(t) = s$ we obtain $a_i \cdot t + \epsilon = 1$
	for some $\epsilon \in \mathfrak{a}$.
	Now let $k \in \NN$ with $\epsilon^k = 0$,
	then calculate
	$1 = (\epsilon + a_i \cdot t)^k
	= \epsilon^k + a_i \cdot t \cdot (\ldots)
	= a_i \cdot t \cdot (\ldots)$,
	so $a_i$ is invertible.

	This proof can clearly be carried out in any geometric theory
	with a surjective ring homomorphism $f : A \to B$
	with nil kernel.
\end{proof}

\begin{remark}
	The ring homomorphism $f : A \to B$
	is then automatically local
	(if $f(x)$ is invertible, then $x$ is invertible),
	as we have seen in the second part of the proof (for $x = a_i$).
\end{remark}
