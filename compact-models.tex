\section{Compact models of the relevant theories}

Because compact models play such a central role
for theories of presheaf type,
we try to identify them
for as many of the theories defined earlier as possible.
Of course, we have not shown all of those theories
to be of presheaf type;
this problem will be handled later as needed.
We start with a very useful lemma.

\begin{lemma}
	Finite colimits of compact objects are compact.
\end{lemma}

\begin{proof}
	Let $G : J \to C$ be a finite diagram
	in a locally small category $C$ with all filtered colimits
	such that all $G(j)$ for $j \in J$ are compact
	and assume that the colimit $\colim_{j \in J} G(j)$ exists.
	Then for any functor $F : I \to C$ with $I$ small and filtered
	we can calculate
	\begin{align*}
		\colim_{i \in I} \Hom(\colim_{j \in J} G(j), F(i))
		\cong\; & \colim_{i \in I} \lim_{j \in J} \Hom(G(j), F(i)) \\
		\cong\; & \lim_{j \in J} \colim_{i \in I} \Hom(G(j), F(i)) \\
		\cong\; & \lim_{j \in J} \Hom(G(j), \colim_{i \in I} F(i)) \\
		\cong\; & \Hom(\colim_{j \in J} G(j), \colim_{i \in I} F(i))
	\end{align*}
	because filtered colimits commute with finite limits in $\Set$.
	And one can check that this composition of bijections
	is indeed the canonical map of Definition \ref{def-compact-object}.
\end{proof}

To determine the compact objects of $\Alg{K}(\Set)$
we first have to describe filtered colimits in that category.

\begin{lemma}
	\label{lemma-filtered-colimits-K-Alg}
	The forgetful functor $U : \Alg{K}(\Set) \to \Set$
	creates filtered colimits.
	Explicitly,
	for any diagram $(A_i)_{i \in I}$ in $\Alg{K}(\Set)$
	with $I$ small and filtered,
	there is a unique $K$-algebra structure on the set
	$A \defeq \colim_{i \in I} U(A_i)$
	making the canonical maps $U(A_i) \to A$
	into $K$-algebra homomorphisms,
	and the resulting cocone in $\Alg{K}(\Set)$ is a colimit.
\end{lemma}

\begin{proof}
	Any two elements $a, a' \in A$
	are the images of elements in some $A_i$ for a common $i \in I$
	since $I$ is filtered.
	So we are forced to define their sum $a + a'$ as the sum in $A_i$
	for $A_i \to A$ to have a chance to be a homomorphism.
	And this is well-defined
	as we can compare the results obtained in $A_i$ and in $A_j$
	in some further $A_k$
	with $i \to k \leftarrow j$ chosen carefully
	such that the two representatives of $a$ in $A_k$ are equal
	and likewise for $a'$.
	This works exactly the same for the product of two elements
	and also for functions of any other arity than two,
	including arity zero for constants (scalars from $K$)
	where we use that $I$ is inhabited
	to find a \enquote{common} $i \in I$
	for the zero elements to be \enquote{combined}.

	The algebraic (equational) axioms for a $K$-algebra are fulfilled
	because each of them involves only finitely many elements
	and therefore every instance can be checked in some $A_i$.

	For another $K$-algebra $A'$
	and compatible homomorphisms $A_i \to A'$
	we only have to check that the induced map of sets $A \to A'$
	is again a homomorphism.
	But this again follows immediatly from the fact that
	sum, product and scalars form $K$
	all involve only finitely many elements
	which can be assumed to lie in a commom $A_i$.
\end{proof}

\begin{remark}
	The proof of Lemma \ref{lemma-filtered-colimits-K-Alg}
	would clearly have worked for any single-sorted algebraic theory.
	For multi-sorted algebraic thories
	we would have to consider the forgetful functor to $\Set^n$ instead
	where $n$ is the number of sorts.
\end{remark}

\begin{definition}
	Let $K$ be a ring.
	\begin{itemize}
		\item
			A $K$-algebra $A$ is \emph{finitely generated}
			if there is a surjective $K$-algebra homomorphism
			\[ K[X_1,\ldots,X_n] \twoheadrightarrow A \]
			for some $n \geq 0$.
		\item
			A $K$-algebra $A$ is \emph{finitely presented}
			if it is isomorphic to one of the form
			\[ K[X_1, \ldots, X_n]/(f_1, \ldots, f_m) \]
			for some $n, m \geq 0$
			and elements $f_1, \ldots, f_m \in K[X_1, \ldots, X_n]$.
	\end{itemize}
\end{definition}

\begin{proposition}
	\label{proposition-compact-K-Alg}
	Let $K$ be a ring.
	The compact objects of $\Alg{K}(\Set)$
	are precisely the finitely presented $K$-algebras.
\end{proposition}

The proof given here is inspired by
the proof of \cite[Theorem 3.12]{adamek-rosicky},
which is a much more general statement.

\begin{proof}
	By Lemma \ref{lemma-filtered-colimits-K-Alg}
	the forgetful functor $\Alg{K}(\Set) \to \Set$
	in particular preserves filtered colimits.
	But this functor is corepresented by the object $K[X]$,
	so $K[X]$ is compact.
	And if $A = K[X_1, \ldots, X_n]/(f_1, \ldots, f_m)$
	is any finitely presented $K$-algebra,
	we can build $A$ from $K[X]$ using only finite colimits:
	The $n$-fold coproduct (tensor product) of $K[X]$ with itself
	is $K[X_1, \ldots, X_n]$
	and then we obtain $A$ as the coequalizer
	\[ \begin{tikzcd}
		K[X_1, \ldots, X_m]
		\ar[r, shift left, "X_i \mapsto f_i"]
		\ar[r, shift right, "X_i \mapsto 0" below]
		&[5pt]
		K[X_1, \ldots, X_n] \ar[r]
		&[-5pt]
		K[X_1,\ldots,X_n]/(f_1,\ldots,f_m) .
	\end{tikzcd} \]

	Now let $A$ be a compact object of $\Alg{K}(\Set)$.
	We first show that $A$ is finitely generated as a $K$-algebra.
	Let $I$ be the set of all finitely generated sub-$K$-algebras of $A$,
	partially ordered by inclusion.
	Then $I$ is filtered as a category, \ie a directed system.
	(The third condition from Definition \ref{def-filtered-category}
	is trivially satisfied for partial orders.)
	Indeed, we have an object $\im (K \to A) \subseteq A$
	(generated by $0$ elements)
	and any two finitely generated subalgebras $A', A'' \subseteq A$
	with $K[X_1, \ldots, X_n] \twoheadrightarrow A'$
	and $K[Y_1, \ldots, Y_m] \twoheadrightarrow A''$
	are contained in
	$\im (K[X_1, \ldots, X_n, Y_1, \ldots Y_m] \to A) \subseteq A$.

	The $K$-algebras $A' \in I$ constitute a diagram in $\Alg{K}(\Set)$
	and the inclusions $A' \hookrightarrow A$ form a cocone.
	We claim that the induced homomorphism
	\[ \colim_{A' \in I} A' \to A \]
	is an isomorphism:
	It is surjective since every $a \in A$
	lies in the subalgebra
	$\im (K[X] \xrightarrow{X \mapsto a} A) \subseteq A$
	finitely generated by the single element $a$.
	And it is injective since
	all the cocone arrows $A' \hookrightarrow A$ are injective
	and any two elements $a' \in A'$, $a'' \in A''$
	can be mapped within $I$
	into a common finitely generated subalgebra of $A$.

	We can now apply the hypothesis that $A$ is compact to the map
	\[ \colim_{A' \in I} \Hom(A, A') \to \Hom(A, \colim_{A' \in I} A')
	\cong \Hom(A, A) \]
	to obtain a preimage of $\id_A$.
	But this is a section $A \to A'$
	of the inclusion $A' \hookrightarrow A$ of some $A' \in I$,
	showing that $A' = A$ and therefore that $A$ is finitely generated.

	Now let $q : K[X_1, \ldots X_n] \twoheadrightarrow A$
	be a fixed surjective $K$-algebra homomorphism.
	We show that the kernel of $q$ is a finitely generated ideal,
	completing the proof of the proposition.
	To this end, let $J$ be the partial order of all
	finitely generated ideals of $K[X_1,\ldots,X_n]$
	contained in $\ker q$.
	This is again a directed system:
	The zero ideal always lies in $J$
	and for two finitely generated ideals
	$(f_1, \ldots, f_m), (g_1, \ldots, g_k) \subseteq \ker q$
	we also have $(f_1, \ldots, f_m, g_1, \ldots, g_k) \subseteq \ker q$.
	The assignment $\mathfrak{a} \mapsto K[X_1,\ldots,X_n]/\mathfrak{a}$
	defines a $J$-shaped diagram in $\Alg{K}(\Set)$
	and the quotient maps $K[X_1,\ldots,X_n]/\mathfrak{a}
	\to K[X_1,\ldots,X_n]/\ker q \cong A$
	induce a homomorphism
	\[ \colim_{\mathfrak{a} \in J} K[X_1,\ldots,X_n]/\mathfrak{a}
	\to A . \]
	This is again an isomorphism:
	It is surjective since already
	$K[X_1,\ldots,X_n]/(0) \to A$ is surjective
	and injective since any $b \in K[X_1,\ldots,X_n]/\mathfrak{a}$
	which becomes zero in $A$
	already becomes zero in $K[X_1,\ldots,X_n]/(\mathfrak{a}+(b))$.

	Using the compactness of $A$ a second time,
	we obtain a preimage of $\id_A$ under the map
	\[ \colim_{\mathfrak{a} \in J} \Hom(A, K[X_1,\ldots,X_n]/\mathfrak{a})
	\to \Hom(A, A) . \]
	This means for some $B \defeq K[X_1,\ldots,X_n]/\mathfrak{a}$
	with $\mathfrak{a} \in J$
	we have a section $s : A \to B$ of the quotient map $q_B : B \to A$.
	We don't have $s \circ q_B = \id_B$ yet,
	but for any $B' = K[X_1,\ldots,X_n]/\mathfrak{b}$
	with $\mathfrak{b} \in J$, $\mathfrak{a} \subseteq \mathfrak{b}$
	we still have $q_{B'} \circ r \circ s = \id_A$
	for the quotient maps $q_{B'} : B' \to A$, $r : B \to B'$
	as in the diagram below.
	\[ \begin{tikzcd}
		B \ar[r, two heads, "q_B" below] \ar[d, two heads, "r" left]
		& A \ar[l, hook', bend right, "s" above] \\
		B' \ar[ru, two heads, "q_{B'}" below right]
	\end{tikzcd} \]
	So our aim is to find $B'$ such that
	$r \circ s \circ q_{B'} = \id_{B'}$.
	For this we remember that $B$ is a finitely presented $K$-algebra,
	so by the first part of this proof it is compact and the map
	\[ \colim_{\mathfrak{b} \in J} \Hom(B, K[X_1,\ldots,X_n]/\mathfrak{b})
	\to \Hom(B, A) \]
	is bijective.
	This time we use injectivity rather than surjectivity:
	$s \circ q_B : B \to B$ and $\id_B$
	considered as elements of the left side
	become equal on the right side:
	$q_B \circ s \circ q_B = q_B = q_B \circ \id_B$.
	Therefore they must be equal already on the left side,
	so there exists $B'$ as above with $r \circ s \circ q_B = r$.
	To see $r \circ s \circ q_{B'} = \id_{B'}$
	we precompose with the surjection $r$:
	$r \circ s \circ q_{B'} \circ r = r \circ s \circ q_B = r$.
	Thus we have $B' \cong A$ and $A$ is finitely presented.
\end{proof}

\begin{remark}
	\label{remark-kernel-of-surjection-to-finitely-presented}
	The last part of the proof showed that
	the kernel of any surjective homomorphism
	$K[X_1,\ldots,X_n] \twoheadrightarrow A$
	is finitely generated if $A$ is compact.
	Combining this with the statement of the proposition,
	any surjection $K[X_1,\ldots,X_n] \twoheadrightarrow A$
	with $A$ a finitely presented $K$-algebra
	has finitely generated kernel.
\end{remark}

We now quickly illustrate Theorem
\ref{theorem-compact-finitely-presented},
which said that every compact model
is presentable by some geometric formula.
For example, $K$ is the initial object of $\Alg{K}(\Set)$,
so it is presented by the formula $\top$ in the empty context.
If we consider $\top$ as a formula in the context $x_1, \ldots, x_n$,
this presents $K[X_1,\ldots,X_n]$ instead:
for elements $a_1,\ldots,a_n$ in any $K$-algebra A
(fulfilling $\top$, \ie no additional condition),
there is a unique $K$-algebra homomorphism $K[X_1,\ldots,X_n] \to A$
sending $X_i$ to $a_i$.
And for a general finitely generated $K$-algebra
$K[X_1,\ldots,X_n]/(f_1,\ldots,f_m)$ we find that it is presented
by the formula $(f_1 = 0) \land \ldots \land (f_m = 0)$
in the context $x_1,\ldots,x_n : A$.
(Here, $f_i$ has to be read as a term built from
the variables $x_1,\ldots,x_n$ and the function symbols of $\Alg{K}$.)
We could have tried to identify
the compact models of $\Alg{K}$ in this way,
but observe that the formulas which occured here
were of an especially simple form (finite conjunctions of equations).
For a more complex geometric formula
it might not be so easy to determine if there even exists a model
presented by that formula.
(In the case of a theory of presheaf type,
the \emph{irreducible} formulas suffice to present all compact models,
see \cite[Theorem 6.1.13]{theories-sites-toposes} for more.)

\begin{lemma}
	\label{lemma-adjoint-functors-compact-objects}
	Let $C$, $D$ be locally small categories with all filtered colimits.
	Let $F : C \to D$ be a functor that has a right adjoint $G : D \to C$
	and assume that $G$ preserves filtered colimits.
	Then $F$ preserves compact objects.
\end{lemma}

\begin{proof}\belowdisplayskip=-12pt
	Let $c \in C$ be compact
	and let $(d_i)_{i \in I}$ be a diagram in $D$
	with $I$ a filtered small category.
	Then we have
	\begin{align*}
		\Hom(F(c), \colim_{i \in I} d_i)
		\;&\cong \Hom(c, G(\colim_{i \in I} d_i)) \\
		&\cong \Hom(c, \colim_{i \in I} G(d_i)) \\
		&\cong \colim_{i \in I} \Hom(c, G(d_i)) \\
		&\cong \colim_{i \in I} \Hom(F(c), d_i).
	\end{align*}
\end{proof}

\begin{lemma}
	\label{lemma-adjoint-functors-K-AlgQuot}
	There are four adjoint functors
	between $\AlgQuot{K}(\Set)$ and $\Alg{K}(\Set)$
	as follows.
	\[ \AlgQuot{K}(\Set) \]
	\[ \begin{tikzcd}[column sep=small]
		\mathfrak{a} \triangleleft A \ar[d, mapsto, ""{name=A}]
		& (0) \triangleleft A' \ar[d, mapsfrom, ""{name=B}]
		& \mathfrak{a} \triangleleft A \ar[d, mapsto, ""{name=C}]
		& (1) \triangleleft A' \ar[d, mapsfrom, ""{name=D}] \\
		A/\mathfrak{a}
		& A'
		& A
		& A'
		\ar[from=A, to=B, phantom, "\dashv"]
		\ar[from=B, to=C, phantom, "\dashv"]
		\ar[from=C, to=D, phantom, "\dashv"]
	\end{tikzcd} \]
	\[ \Alg{K}(\Set) \]
\end{lemma}

\begin{proof}
	This is easy to check.
\end{proof}

\begin{lemma}
	\label{lemma-filtered-colimits-K-AlgQuot}
	The forgetful functor
	$\AlgQuot{K}(\Set) \to \Alg{K}(\Set),
	(\mathfrak{a} \triangleleft A) \mapsto A$
	creates filtered colimits.
	Explicitly,
	for any diagram $(\mathfrak{a}_i \triangleleft A_i)_{i \in I}$
	in $\AlgQuot{K}(\Set)$ with $I$ small and filtered
	the colimit $A \defeq \colim_{i \in I} A_i$
	computed in $\Alg{K}(\Set)$
	becomes a colimit in $\AlgQuot{K}(\Set)$
	by taking as ideal $\mathfrak{a} \triangleleft A$
	the union of the images of all $\mathfrak{a}_i$.
\end{lemma}

\begin{proof}
	The union of the images of $\mathfrak{a}_i$ in A
	is indeed an ideal:
	The sum of elements from $\mathfrak{a}_i$ and $\mathfrak{a}_j$
	can be computed in any $A_k$ with $i \to k \leftarrow j$,
	where they both lie in $\mathfrak{a}_k$.
	And for any object
	$(\mathfrak{a}' \triangleleft A') \in \AlgQuot{K}(\Set)$,
	and compatible morphisms $f_i : (\mathfrak{a}_i \triangleleft A_i)
	\to (\mathfrak{a}' \triangleleft A')$
	inducing $f : A \to A'$
	we have $f(\mathfrak{a}) \subseteq \mathfrak{a}'$
	because any element of $\mathfrak{a}$
	is represented by an element of some $\mathfrak{a}_i$
	and $f_i(\mathfrak{a}_i) \subseteq \mathfrak{a}'$.
\end{proof}

\begin{lemma}
	\label{lemma-compact-K-AlgQuot}
	An object $(\mathfrak{a} \triangleleft A) \in \AlgQuot{K}$ is compact
	if and only if $A$ is a finitely presented $K$-algebra
	and $\mathfrak{a}$ is a finitely generated ideal.
\end{lemma}

\begin{proof}
	The functor $(\mathfrak{a} \triangleleft A) \mapsto A/\mathfrak{a}$
	from Lemma \ref{lemma-adjoint-functors-K-AlgQuot}
	preserves compact objects
	by Lemma \ref{lemma-adjoint-functors-compact-objects}
	because is right adjoint $A' \mapsto ((0) \triangleleft A')$
	has a further right adjoint,
	so it even preserves all colimits.
	The functor $(\mathfrak{a} \triangleleft A) \mapsto A$
	also preserves compact objects,
	because its right adjoint $A' \mapsto ((1) \triangleleft A')$
	preserves filtered colimits
	by the description of filtered colimits in $\AlgQuot{K}(\Set)$
	given in Lemma \ref{lemma-filtered-colimits-K-AlgQuot}.
	(Note that this functor does not preserve all colimits
	as it does not preserve the initial object,
	which is $K$ in $\Alg{K}(\Set)$
	and $(0) \triangleleft K$ in $\AlgQuot{K}(\Set)$.)
	So if $\mathfrak{a} \triangleleft A$ is compact,
	then both $A$ and $A/\mathfrak{a}$
	are finitely presented $K$-algebras.
	To see that $\mathfrak{a}$ is finitely generated,
	observe that the kernel of a composite
	\[ K[X_1,\ldots,X_n] \twoheadrightarrow A
	\twoheadrightarrow A/\mathfrak{a} \]
	is finitely generated by Remark
	\ref{remark-kernel-of-surjection-to-finitely-presented}
	and its image in $A$ is $\mathfrak{a}$.
	This completes the \enquote{only if} part.

	For the \enquote{if} part
	we start by observing that the composite forgetful functor
	\[ \AlgQuot{K}(\Set) \to \Alg{K}(Set) \to \Set, \quad
	(\mathfrak{a} \triangleleft A) \mapsto A \]
	preserves filtered colimits,
	so the object $(0) \triangleleft K[X]$ corepresenting it
	is compact.
	Just like in the proof of Lemma \ref{lemma-filtered-colimits-K-Alg}
	we can build fom $(0) \triangleleft K[X]$ any $(0) \triangleleft A$
	with $A$ finitely presented using only finite colimits.
	Next we see that also the functor
	\[ \AlgQuot{K}(\Set) \to \Set, \quad
	(\mathfrak{a} \triangleleft A) \mapsto A \]
	preserves filtered colimits,
	by the description in Lemma \ref{lemma-filtered-colimits-K-AlgQuot}.
	This functor is corepresented by $(X) \triangleleft K[X]$,
	which is therefore compact.
	And also, as a finite coproduct,
	$(X_1,\ldots,X_n) \triangleleft K[X_1,\ldots,X_n]$.
	Now for any finitely generated ideal
	$(f_1,\ldots,f_n) \triangleleft A$
	we conclude that the pushout
	\[ \begin{tikzcd}
		(0) \triangleleft K[X_1,\ldots,X_n]
		\ar[r, "X_i \mapsto f_i"] \ar[d]
		\ar[dr, phantom, "\lrcorner" very near end]
		& (0) \triangleleft A \ar[d] \\
		(X_1,\ldots,X_n) \triangleleft K[X_1,\ldots,X_n] \ar[r]
		& (f_1,\ldots,f_n) \triangleleft A
	\end{tikzcd} \]
	is compact.
\end{proof}

\begin{proposition}
	\label{proposition-compact-K-AlgNilQuot}
	An object $(\mathfrak{a} \triangleleft A) \in \AlgNilQuot{K}(\Set)$
	is compact if and only if
	it is compact as an object of $\AlgQuot{K}(\Set)$,
	that is, if and only if
	$A$ is a finitely presented $K$-algebra
	and $\mathfrak{a}$ is finitely generated.
\end{proposition}

\begin{proof}
	$\AlgNilQuot{K}(\Set)$ is a full subcategory of $\AlgQuot{K}(\Set)$
	closed under filtered colimits
	(again by the description in
	Lemma \ref{lemma-filtered-colimits-K-AlgQuot}).
	Therefore, if $\mathfrak{a} \triangleleft A$
	is compact in $\AlgQuot{K}(\Set)$,
	it is in particular compact in $\AlgNilQuot{K}(\Set)$.

	For the converse,
	we consider adjoint functors similar to those from
	Lemma~\ref{lemma-adjoint-functors-K-AlgQuot}:
	\[ \AlgNilQuot{K}(\Set) \]
	\[ \begin{tikzcd}[column sep=small]
		\mathfrak{a} \triangleleft A \ar[d, mapsto, ""{name=A}]
		& (0) \triangleleft A' \ar[d, mapsfrom, ""{name=B}]
		& \mathfrak{a} \triangleleft A \ar[d, mapsto, ""{name=C}]
		& \Nil(A') \triangleleft A' \ar[d, mapsfrom, ""{name=D}] \\
		A/\mathfrak{a}
		& A'
		& A
		& A'
		\ar[from=A, to=B, phantom, "\dashv"]
		\ar[from=B, to=C, phantom, "\dashv"]
		\ar[from=C, to=D, phantom, "\dashv"]
	\end{tikzcd} \]
	\[ \Alg{K}(\Set) \]
	Only the rightmost functor had to be adjusted,
	because $(1) \triangleleft A'$ is not a nil ideal
	(unless $A'$ is the zero ring).
	One can easily check
	that $A' \mapsto (\Nil(A') \triangleleft A')$ is a functor,
	that it is indeed right adjoint to
	$(\mathfrak{a} \triangleleft A) \mapsto A$
	and that it preserves filtered colimits.
	% TODO: more explanation?
	So as before we can conclude
	by Lemma~\ref{lemma-adjoint-functors-compact-objects}
	that for $\mathfrak{a} \triangleleft A$
	compact in $\AlgNilQuot{K}(\Set)$,
	$A/\mathfrak{a}$ and $A$ are finitely presented $K$-algebras
	and $\mathfrak{a}$ is finitely generated.
\end{proof}

We now turn to the theories with two sorts.

\begin{lemma}
	\label{lemma-colimits-K-Alg-R-Alg}
	The category $\AlgsAlg{K}{R}(\Set)$ has all small colimits.
	Specifically, for $(A_i \to B_i)_{i \in I}$
	a small diagram in $\AlgsAlg{K}{R}(\Set)$,
	the colimit is given by the canonical $K$-algebra homomorphism
	\[ \colim_{i \in I} A_i \to \colim_{i \in I} B_i, \]
	where the first colimit is to be computed in $\Alg{K}(\Set)$
	and the second in $\Alg{R}(\Set)$.
\end{lemma}

\begin{proof}
	By the canonical homomorphism we mean the one
	induced by the composites of $A_i \to B_i$
	with $B_i \to \colim_{i \in I} B_i$,
	which indeed form a cocone of $K$-algebra homomorphisms.
	For a cocone $((A_i \to B_i) \to (A \to B))_{i \in I}$,
	we get an induced $K$-algebra homomorphism $\colim_i A_i \to A$
	and an $R$-algebra homomorphism $\colim_i B_i \to B$.
	These form a morphism in $\AlgsAlg{K}{R}(\Set)$,
	\ie the right square in the diagram (in $\Alg{K}(\Set)$)
	\[ \begin{tikzcd}
		A_i \ar[r] \ar[rr, bend left] \ar[d]
		& \colim_i A_i \ar[r, dashed] \ar[d]
		& A \ar[d] \\
		B_i \ar[r] \ar[rr, bend right]
		& \colim_i B_i \ar[r, dashed]
		& B
	\end{tikzcd} \]
	kommutes, since this can be tested
	by precomposing with all $A_i \to \colim_i A_i$.
	This proof can in fact be carried out
	in the general setting of a comma category $(C \downarrow F)$
	with $F : D \to C$ any functor
	such that both $C$ and $D$ have all small colimits.
\end{proof}

\begin{proposition}
	\label{proposition-compact-K-Alg-R-Alg}
	An object $(A \to B) \in \AlgsAlg{K}{R}(\Set)$
	is compact if and only if
	$A$ is a finitely presented $K$-algebra
	and $B$ is a finitely presented $R$-algebra.
\end{proposition}

\begin{proof}
	We first want to show that
	$A$ must be finitely presented as a $K$-algebra,
	using again Lemma \ref{lemma-adjoint-functors-compact-objects}.
	So let
	\[ F_1 : \AlgsAlg{K}{R}(\Set) \to \Alg{K}(\Set),
	\quad (A \to B) \mapsto A \]
	be the forgetful functor.
	It has a right adjoint, namely
	\[ G_1 : \Alg{K}(\Set) \to \AlgsAlg{K}{R}(\Set),
	\quad A \mapsto (A \to 0). \]
	Now, $G_1$ does not (in general) preserve all small colimits,
	as it doesn't preserve (in general) the initial object,
	which would be $K \to R$ in $\AlgsAlg{K}{R}(\Set)$.
	But it does preserve filtered colimits
	(and indeed, all inhabited colimits),
	as we can see from Lemma \ref{lemma-colimits-K-Alg-R-Alg}:
	if all $B_i$ are $0$, and there is at least one $i \in I$,
	then $\colim_{i \in I} B_i = 0$.

	Now consider the other forgetful functor
	\[ F_2 : \AlgsAlg{K}{R}(\Set) \to \Alg{R}(\Set),
	\quad (A \to B) \mapsto B. \]
	We can again find a right adjoint,
	\[ G_2 : \Alg{R}(\Set) \to \AlgsAlg{K}{R}(\Set),
	\quad B \mapsto (B \xrightarrow{=} B). \]
	To see that $G_2$ preserves filtered colimits,
	we remind ourselves of Lemma \ref{lemma-filtered-colimits-K-Alg},
	which implies that filtered colimits are computed in
	$\Alg{K}(\Set)$ and in $\Alg{R}(\Set)$ in the same way
	(\ie, $\Alg{R}(\Set) \to \Alg{K}(\Set)$ preserves filtered colimits),
	so we are done by Lemma \ref{lemma-colimits-K-Alg-R-Alg}.
	We conclude that for compact $(A \to B) \in \AlgsAlg{K}{R}(\Set)$,
	$A$ must be compact in $\Alg{K}(\Set)$
	and $B$ must be compact in $\Alg{R}(\Set)$,
	which is one half of the claim.

	For the other half, consider the composition of $F_1$ and $F_2$
	with the respective underlying-set functors.
	They both preserve filtered colimits,
	and they are represented by $K[X] \xrightarrow{X \mapsto X} R[X]$
	respectively $K \to R[Y]$.
	These are the building blocks which will suffice
	to obtain all of the objects from the statement
	using only finite colimits.
	So let $f : A \to B$ be an object with
	$A = K[X_1, \ldots, X_n]/(f_1, \ldots, f_m)$,
	$B = R[Y_1, \ldots, Y_k]/(g_1, \ldots, g_l)$.
	Start by taking the coproduct of $n$ copies of $K[X] \to R[X]$
	and $m$ copies of $K \to R[Y]$, yielding
	\[ K[X_1, \ldots, X_n] \to R[X_1, \ldots, X_n, Y_1, \ldots, Y_k]. \]
	Forming a coequalizer for each $f_i$ (using $K[X] \to R[X]$)
	and each $g_i$ (using $K \to R[Y]$),
	we have
	\[ A \to A \otimes_K B, \]
	and it only remains to identify each $X_i$ with $f(X_i)$
	on the right side
	by another $n$ coequalizers, again using $K \to R[Y]$.
\end{proof}

\begin{lemma}
	\label{lemma-inhabited-colimits-K-Alg-R-Quot}
	The full subcategory $\AlgsQuot{K}{R}(\Set)$
	of $\AlgsAlg{K}{R}(\Set)$ is closed under inhabited small colimits.
\end{lemma}

\begin{proof}
	Let $(A_i \twoheadrightarrow B_i)_{i \in I}$
	be a small diagram in $\AlgsQuot{K}{R}(\Set)$
	with $I$ inhabited.
	Let $b \in \colim_i B_i \cong (\bigotimes_i B_i)/{\sim}$
	be any element.
	Then $b$ can be written as a sum of products
	of elements coming from some $B_i$,
	\ie we find a polynomial $p \in \ZZ[X_1, \ldots, X_n]$
	and elements $b_j \in B_{i_j}$
	such that $p(b_1, \ldots, b_k) = b$ in $\colim_i B_i$.
	(Here we need $I$ to be inhabited to get rid of any scalars from $R$.)
	But for these $b_j$ we can find preimages $a_j \in A_{i_j}$
	under the maps $A_{i_j} \to B_{i_j}$,
	and then $p(a_1, \ldots, a_n)$ is a preimage of $b$
	under $\colim_i A_i \to \colim_i B_i$,
	which is therefore surjective.
\end{proof}

\begin{remark}
	The subcategory $\AlgsQuot{K}{R}(\Set)$ is, in general,
	not closed under arbitrary small colimits.
	For instance, the initial object $K \to R$ of $\AlgsAlg{K}{R}(\Set)$
	doesn't have to lie in $\AlgsQuot{K}{R}(\Set)$.
\end{remark}

\begin{corollary}
	\label{corollary-compact-K-Alg-R-Quot}
	An object $(A \twoheadrightarrow B) \in \AlgsQuot{K}{R}(\Set)$
	is compact if and only if
	it is compact as an object of $\AlgsAlg{K}{R}(\Set)$,
	that is, if and only if
	$A$ is a finitely presented $K$-algebra
	and $B$ is a finitely presented $R$-algebra.
\end{corollary}

\begin{proof}
	The subcategory $\AlgsQuot{K}{R}(\Set)$ is in particular
	closed under filtered colimits in $\AlgsAlg{K}{R}(\Set)$
	by Lemma \ref{lemma-inhabited-colimits-K-Alg-R-Quot}.
	So if an object $(A \twoheadrightarrow B)$ of $\AlgsQuot{K}{R}(\Set)$
	is compact in $\AlgsAlg{K}{R}(\Set)$,
	then it is also compact in $\AlgsQuot{K}{R}(\Set)$.
	Furthermore, the functors $G_1$, $G_2$ in the proof of 
	Proposition \ref{proposition-compact-K-Alg-R-Alg}
	factorize over $\AlgsQuot{K}{R}(\Set)$,
	as $(A \to 0)$ and $B \xrightarrow{=} B$ are always surjective.
	So they are right adjoint to the restrictions of $F_1$, $F_2$
	(and still preserve filtered colimits),
	which provides the other implication just like before.
\end{proof}

\begin{remark}
	Note that for a compact model
	$(f : A \twoheadrightarrow B) \in \AlgsQuot{K}{R}(\Set)$,
	the kernel of $f$ doesn't have to be a finitely generated ideal,
	if $R$ is not a finitely presented $K$-algebra.
	For example, we can take $A = K$, $B = R$
	with $K = \ZZ[X_1, X_2, \ldots] \xrightarrow{X_i \mapsto 0} R = \ZZ$.
\end{remark}

\begin{lemma}
	\label{lemma-filtered-colimits-K-Alg-R-NilQuot}
	The full subcategory $\AlgsNilQuot{K}{R}(\Set)$
	of $\AlgsQuot{K}{R}(\Set)$
	is closed under filtered colimits.
\end{lemma}

\begin{proof}
	Let $(f_i : A_i \to B_i)_{i \in I}$ be a filtered small diagram
	in $\AlgsNilQuot{K}{R}(\Set)$
	and let $a \in \colim_i A_i$ be an element
	which gets mapped to zero in $\colim_i B_i$.
	(One colimit being computed in $\Alg{K}(\Set)$,
	the other in $\Alg{R}(\Set)$.)
	We use the description of these filtered colimits
	given in Lemma \ref{lemma-filtered-colimits-K-Alg} as follows.
	There is some $a' \in A_i$ representing $a$ for some $i \in I$,
	and $f_i(a') \in B_i$ represents zero in $\colim_i B_i$.
	But this means that there is an arrow $i \to j$ in $I$
	such that $f_i(a')$ is zero in $B_j$.
	That is, $a'$ gets mapped to some $a'' \in A_j$ with $f_j(a'') = 0$.
	Since $f_j : A_j \to B_j$ is an object of $\AlgsNilQuot{K}{R}$,
	this means that $a''$ is nilpotent,
	and since $a''$ also represents $a$, the latter was nilpotent.
\end{proof}

\begin{corollary}
	\label{corollary-K-Alg-R-NilQuot-implies-compact}
	Let $A \twoheadrightarrow B$ be an object
	of $\AlgsNilQuot{K}{R}(\Set)$
	with $A$ a finitely presented $K$-algebra
	and $B$ a finitely presented $R$-algebra.
	Then $A \twoheadrightarrow B$ is compact.
\end{corollary}

\begin{proof}
	This follows from Lemma \ref{lemma-filtered-colimits-K-Alg-R-NilQuot}
	just like before.
\end{proof}

For the converse of
Corollary \ref{corollary-K-Alg-R-NilQuot-implies-compact},
let's consider the pairs of adjoint functors
$F_1 \dashv G_1$ and $F_2 \dashv G_2$ from the proof of
Proposition \ref{proposition-compact-K-Alg-R-Alg} again.
We have that $G_2$ factors over $\AlgsNilQuot{K}{R}(\Set)$,
since $B \xrightarrow{=} B$ has kernel $(0)$.
But the kernel of $G_1(A) = (A \to 0)$
is certainly not a nil ideal in general.
It doesn't seem obvious how to modify $G_1$
to get a new right adjoint.
Instead, we will obtain the converse of
Corollary \ref{corollary-K-Alg-R-NilQuot-implies-compact}
in a completely different manner in
Corollary \ref{corollary-compact-K-Alg-R-NilQuot}.
