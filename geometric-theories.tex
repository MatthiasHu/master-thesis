\section{Geometric theories}

\begin{definition}
	A \emph{geometric theory} consists of
	a set of \emph{sorts},
	a set of \emph{function symbols},
	each equipped with
	a finite (possibly empty) list $A_1,\ldots,A_n$ of \emph{input sorts}
	and one \emph{output sort} $B$,
	denoted
	\[ f : A_1,\ldots,A_n \to B, \]
	furthermore a set of \emph{relation symbols},
	each equipped with
	a finite (possibly empty) list of sorts $A_1,\ldots,A_n$,
	denoted
	\[ R \rightarrowtail A_1,\ldots,A_n, \]
	and finally a set of \emph{axioms},
	each a sequent of the form
	\[ \phi \turnstile{x_1 : A_1, \ldots, x_n : A_n} \psi, \]
	where $\phi$ and $\psi$ are geometric formulas
	in the context $x_1 : A_1, \ldots, x_n : A_n$
	over the \emph{signature} given by
	the sorts, function symbols and relation symbols of the theory.
\end{definition}

We don't go into the details of defining
\emph{terms} and \emph{formulas} here
(see \cite[Section D.1.1]{elephant} instead)
but just want to mention that the \emph{geometric formulas}
are those (infinitary) first-order formulas
which only use the logical connectives
\[ {\top}, {\bot}, {\land}, {\lor}, {\bigvee}, {\exists}, \]
but not ${\Rightarrow}, {\bigwedge}, {\forall}$.

Also, we don't give a complete definition of a \emph{model}
of a theory $T$ in a topos $\E$.
% TODO: define topos
(And we will not need the notion of a model in more general categories.)
But recall that if we are given
a structure $M$ consisting of
an object $A_M$ for every sort $A$,
a morphism $f_M : A_1 \times \ldots \times A_n \to B_M$
for every function symbol
and a subobject $R_M \hookrightarrow A_1 \times \ldots \times A_n$
for every relation symbol,
then we can inductively (over the structure of the formula) define
the \emph{interpretation} of a formula $\phi$
in a context $x_1 : A_1, \ldots, x_n : A_n$,
which is a subobject
\[ \interpretation{\phi}_M \hookrightarrow
M_{A_1} \times \ldots \times M_{A_n}. \]
Then a sequent $\phi \turnstile{C} \psi$ is \emph{fulfilled}
if $\interpretation{\phi}_M \leq \interpretation{\psi}_M$ as subobjects
and $M$ is a \emph{model} of $T$ if all axioms are fulfilled for $M$.
Finally, a homomorphism of such structures $M$, $N$
for the same signature
(this does not depend on the axioms of $T$)
consists of morphisms $A_M \to A_N$ for each sort $A$,
which are compatible with the
morphisms assigned to function symbols
and the subobjects assigned to relation symbols.
For details see \cite[Section D1.2]{elephant}.

We will of course need the following Soundness Theorem.
For the notion of provability
we refer to \cite[Section D.1.3]{elephant}.

\begin{proposition}[Soundness Theorem]
	\label{proposition-soundness}
	Let $M$ be a model of a geometric theory $T$
	in a topos $\E$ (or in any geometric category).
	Then any geometric sequent which is provable in $T$
	is also fulfilled in $M$.
\end{proposition}

\begin{proof}
	See \cite[Proposition 1.3.2]{elephant}.
\end{proof}

\begin{definition}
	\begin{itemize}
		\item
			A geometric theory $T$ is \emph{algebraic}
			if it has no relation symbols
			and all axioms of $T$ are of the form
			\[ \top \turnstile{C} s = t \]
			where $s$ and $t$ are terms in the context $C$
			and $C$ contains no other variables than
			those occuring in $s$ or in $t$.
		\item
			A geometric theory $T$ is \emph{cartesian}
			if the only logical connectives occuring in its axioms
			(left and right of the turnstile $\vdash$)
			are ${\top}, {\land}, {\exists}$
			and the axioms of $T$ can be given
			a well-founded partial ordering
			such that every occurence of existential quantification
			$(\ex{x}{A} \phi)$ in an axiom $\sigma$
			can be shown to refer to unique existence
			($\phi \land \phi[y/x] \turnstile{C, x, y} x = y$,
			where $C$ is the context in which $(\ex{x}{A} \phi)$ occured)
			relative to the axioms preceding $\sigma$.
	\end{itemize}
\end{definition}

Of course, any algebraic theory is cartesian.

\begin{definition}
	A \emph{quotient} of a geometric theory $T$
	is another geometric theory $T'$ over the same signature
	such that all axioms of $T$ are provable in $T'$.
\end{definition}
